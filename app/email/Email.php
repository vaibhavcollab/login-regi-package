<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dell
 * Date: 3/4/17
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */

namespace App\email;
use Illuminate\Support\Facades\Mail;

class Email {
    /*
     * Basic Mail send to person
     * @param: to- string, name - string, subject - string
     */
    public function basic_email($to, $name, $subject, $attachment) {
        $data = array('name'=>$name, 'to' => $to, 'subject' => $subject, 'attachment' => $attachment);

        Mail::send('mail.mail', $data, function($message) use ($data){
            $message->to($data['to'], 'Tutorials Point')->subject($data['subject']);
            $message->cc('shyampatil.testing@gmail.com');

            if(!empty($data['attachment'])) {
                foreach($data['attachment'] as $attachmentDetail) {
                    $message->attach($attachmentDetail);
                }
            }
        });
        return;
    }

    public function content_email1($to, $name, $subject, $attachment, $content) {
        $data = array('name'=>$name, 'to' => $to, 'subject' => $subject, 'attachment' => $attachment, 'content' => $content);

        Mail::send([], $data, function ($message) use ($data) {
            $message->to($data['to'])
                ->subject($data['subject']);

            if(!empty($data['attachment'])) {
                foreach($data['attachment'] as $attachmentDetail) {
                    $message->attach($attachmentDetail);
                }
            }

            $message->setBody($data['content'], 'text/html');
        });
        return;
    }

    public function send_verification_mail($to, $name, $subject, $attachment,$user_id, $user_name, $token) {

        $content = '<!DOCTYPE html>
                    <html>
                    <head>
                        <meta charset="utf-8" />
                        <title>Please verify your email address</title>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    </head>
                    <body>
                    <div>
                        <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"><img style="border: 0;-ms-interpolation-mode: bicubic;display: block;Margin-left: auto;Margin-right: auto;max-width: 152px" src="'. url("images/logo.jpg") .'" alt="" width="68" height="42"></div>
                        <div align="center"><h3>Please verify your email address</h3></div>
                        <hr>
                        <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">
                        Hi  '.$user_name.',</p>
                        <p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px"> Please verify your email address so we know that it\'s really you! </p>
                        <a style="font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block" shape="rect" href="'. url("/user/activation/$token") .'" target="_blank">
                            <span class="il">Verify</span> my email address
                        </a>
                    </div>
                    </body>
                    </html>';

        $data = array('name'=>$name, 'to' => $to, 'subject' => $subject, 'attachment' => $attachment, 'content' => $content);

        Mail::send([], $data, function ($message) use ($data) {
            $message->to($data['to'])
                ->subject($data['subject']);

            if(!empty($data['attachment'])) {
                foreach($data['attachment'] as $attachmentDetail) {
                    $message->attach($attachmentDetail);
                }
            }

            $message->setBody($data['content'], 'text/html');
        });
        return;
    }

    static function sendEmail($config, $data) {

        //print_r($data);exit;
        /*Mail::send($config['view'], $data, function($message) use ($config){
            $message
                ->to($config['email'], 'Tutorials Point')
                ->subject($config['subject']);

            if (isset($config['cc']) && $config['cc'] && env('APP_DEBUG') == 'true') {
                $message->cc($config['cc']);
            }

            if (isset($config['bcc']) && $config['bcc'] && env('APP_DEBUG') != 'true') {

                $message->bcc($config['bcc'], 'Localhost');
            }

            if (isset($config['attachment_path']) && $config['attachment_path']) {
                $message->attach($config['attachment_path']);
            }
        });*/
        self::sendEmailUsingMailJet();
        return;
    }

    public static function content_email($to, $name, $subject, $attachment, $content, $ccemails = NULL, $from = 'messages@sharedmachine.in', $fromName = 'Shared Machine') {
        if(!is_array($to)) {
            $toEmails = [['Email' => $to, 'Name' => $name]];
        } else {
            foreach($to as $toE) {
                $toEmails[] = ['Email' => $toE, 'Name' => $name];
            }
        }
        $toccEmails = array();
        if (!empty($ccemails)) {
            $toccemail = explode(',', $ccemails);
            foreach ($toccemail as $tocc) {
                $toccEmails[] = ['Email' => $tocc, 'Name' => " "];
            }
        }

        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $from,
                        'Name' => $fromName
                    ],
                    /*'To' => [
                        [
                            'Email' => "thakursandeep777@gmail.com",
                            'Name' => "Sandeep"
                        ]
                    ],*/
                    'To' => $toEmails,
                    'Cc' => $toccEmails,
                    'Subject' => $subject,
                    'HTMLPart' => $content
                ]
            ]
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.mailjet.com/v3.1/send");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );
        curl_setopt($ch, CURLOPT_USERPWD, "54d68eee785dd12dafa1e28a52522b67:663b7734ec85410500fe17cf8e0d720a");
        $server_output = curl_exec($ch);
        curl_close ($ch);

        $response = json_decode($server_output);
        return $response;
    }
}
