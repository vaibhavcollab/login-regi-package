<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  
    <script>
        var APP_URL = "<?php echo url('/'); ?>";
    </script>
    <script src="<?php echo asset('/') . 'public-ui/js/jquery.min.js';  ?>"></script><!-- 2.1.3v -->
    <!-- <script src="<?php echo asset('/') . 'public-ui/js/jquery-3.4.1.slim.min.js';  ?>" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <script src="<?php echo asset('/') . 'public-ui/js/popper.min.js';  ?>" ></script>
    <script src="<?php echo asset('/') . 'public-ui/js/bootstrap.min.js';  ?>" ></script>
    <!-- Fonts -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:200,300,400,600%7CRoboto:300,400&amp;display=swap" media="all"> -->
    <link rel="stylesheet" href="<?php echo asset('/') . 'public-ui/css/bootstrap.min.css';  ?>" >

    <!-- For Listing Slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo asset('/') . 'public-ui/css/swiper.min.css';  ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo asset('/') . 'public-ui/css/easyzoom.css';  ?>">
    <script src="<?php echo asset('/') . 'public-ui/js/swiper.min.js';  ?>"></script>
    <script src="<?php echo asset('/') . 'public-ui/js/easyzoom.js';  ?>"></script>
    <!-- END Listing Slider -->

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo asset('/') . 'public-ui/css/custom.css';  ?>" rel="stylesheet">

    <script src="{{ asset('js/tinymce.min.js') }}" referrerpolicy="origin"></script>

    <!-- Google Api -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_QNJ9wsUGcYf2uLotBmTprhwCM0f3ycE&libraries=places"></script> -->
    <script type="text/javascript" src="<?php echo asset('/') . 'public-ui/js/google-location.js'; ?>"></script>
    <!-- <script type="text/javascript">
        <!-- var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"a2c6ea68ad7c45909f57fd92e7567b26943231319abcf1c85df7c93034c2146b0bed0e499410a25afb70ba801342bcbe", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>"); -->
    </script>
    <script>
        // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        //     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        //     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        // })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        // ga('create', 'UA-63935099-6', 'auto');
        // ga('set', 'anonymizeIp', true);
        // ga('send', 'pageview');
    </script>
    <script type="text/javascript">



        jQuery(document).ready(function($) {

            $(window).on('scroll',function(){
                var sticky = $('.sticky_top_keyService'),
                    scroll = $(window).scrollTop();

                if (scroll >= 100)
                    {
                        console.log('0000',scroll);
                        sticky.addClass('fixed');
                    }else {
                        console.log('1111',scroll);
                        sticky.removeClass('fixed');
                    }
            });

            $(".scroll").click(function(event){
                event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
    <link rel="stylesheet" href="{{asset('jquery-ui-1.12.1/jquery-ui.css')}}">
    <script src="{{asset('jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    <script src="{{asset('public-ui/js/sm.js')}}"></script>
    <!-- <script>
        $(document).ready(function() {
            rental_category_search();
            resale_category_search();
            rental_search_submit();
            resale_search_submit();
        });
    </script> -->
    <link rel="stylesheet" href="{{asset('jquery-ui-1.12.1/jquery-ui.css')}}">
    <script src="{{asset('jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    {{--@yield('extra-css')
    @yield('extra-js') --}}

</head>


<!-- END Google Api -->
<body onload="initAutocomplete()" class="application">
  <div class="loader" style="display: none;"></div>

  <nav id="topnavbar" class="navbar navbar-expand-lg topbar background_orange">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item active">
                <a class="nav-link topbar-item" href="#"><i class="fa fa-mobile" style="font-size: 19px;" aria-hidden="true"></i> App download </a>
            </li>
            <li class="nav-item">
                <a class="nav-link topbar-item" href="#"><i class="fa fa-phone" aria-hidden="true"></i> {{ __('app.PHONE_NO') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link topbar-item" href="mailto:sales@sharedmachine.in" class="mailto"><i class="fa fa-envelope-o" aria-hidden="true"></i> sales@sharedmachine.in</a>
            </li>
            <li class="nav-item">
                <a class="nav-link topbar-item" href="#"><i class="fa fa-language" aria-hidden="true"></i> {{ __('app.APP_LANG') }} &#9662;</a>
                <ul class="dropdown">
                    <li class="languaage_li"><a href="{{ url('locale/en') }}">English </a></li>
                    <li class="languaage_li"><a href="{{ url('locale/hn') }}">Hindi </a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

<nav id="first_menu" class="navbar navbar-expand-lg navbar-light bg-light p-0 navbar-sm-menus sticky-top">
    <a class="navbar-brand topbar-item p-0" href="{{url('/')}}" style="margin-left: 10px;">
        <img src="<?php echo asset('/') . 'public-ui/images/';  ?>logo.png">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-2" aria-controls="navbarSupportedContent-2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

  <!--   <div class="collapse navbar-collapse" id="navbarSupportedContent-2" style="margin-left: 20px;">
        <ul class="navbar-nav mr-auto">
            <!-- Authentication Links


                {{-- @foreach(App\common\Common::getMenu('PUBLIC_HOME') as $key => $menu)
                @if(isset($menu['parent_child']))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text_orange" role="button" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">{{ App\common\Common::get_translation($menu['parent_child']['parent']['language_key'],$menu['parent_child']['parent']['description'])}}</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            @if(!empty($menu['parent_child']['child']))
                                @foreach($menu['parent_child']['child'] as $childMenu)
                                    <a data-menu_id="{{$childMenu['menu_id']}}" data-menu_mapping_id="{{$childMenu['menu_mapping_id']}}" class="dropdown-item" href="{{url($childMenu['url'])}}">{{$childMenu['description']}}</a>
                                @endforeach
                            @endif
                        </div>
                    </li>
                @else
                    <li class="nav-item  menu-item active">
                        <a class="nav-link text_orange" data-menu_id="{{$menu['parent']['id']}}" href="{{url($menu['parent']['url'])}}">{{$menu['parent']['description']}}</a>
                    </li>
                @endif
             @endforeach  --}}

        </ul>
    </div>-->

    <?php
        // added by shrikant 11-Sept-20.
        // $session_data = session()->get('user_info');
        $url = 'my_dashboard';

        // if(isset($session_data) && !empty($session_data)){
        //     switch ($session_data['company_id']) {
        //         case 0:
        //             $url = 'super_admin';break;
        //         case 5:
        //             $url = 'collab_team';break;
        //         case $session_data['company_id'] > 1000:
        //             $url = 'my_dashboard'; break;
        //     }
        // }

    ?>

    <div class="collapse navbar-collapse" id="navbarSupportedContent1"  style="margin-right:18px;">
        <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item  menu-item active">
                <a class="btn my-2 my-sm-0 btn-sm background_orange text_white" href="{{url($url)}}" style="padding: 1px 5px 0px 5px;">My Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    @if (isset(auth()->user()->profile_pic) && !empty(auth()->user()->profile_pic))
                        <img src="{{ url(auth()->user()->profile_pic) }}" style="width: 24px; height: 24px; border-radius: 50%;">
                        <span style="color:black;font-weight:400;">{{ Auth::user()->name }}</span>
                    @else
                        <i class="fa fa-user text_black" aria-hidden="true"></i>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    @guest
                        <a class="dropdown-item" href="<?php echo URL::to('/login'); ?>">Login</a>
                        <a class="dropdown-item" href="<?php echo URL::to('/register'); ?>">Registration</a>
                    @else
                        <a class="dropdown-item" href="{{ url("user_profile") }}">
                           <img src="<?php echo asset('/') . 'public-ui/images/My-Profile.png'; ?>"> Profile
                        </a>

                        <a class="dropdown-item" href="{{url($url)}}">
                           <img src="<?php echo asset('/') . 'public-ui/images/My-Dashboard.png'; ?>"> Dashboard
                        </a>

                        <a class="dropdown-item" href="{{ url("my_wishlist") }}">
                            <img src="<?php echo asset('/') . 'public-ui/images/Favourite.png'; ?>"> Wishlist
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();localStorage.clear();
                                document.getElementById('logout-form').submit();">
                            <img src="<?php echo asset('/') . 'public-ui/images/Logut1.png'; ?>"> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </div>
            </li>
        </ul>
    </div>
</nav>


<div class="page-content">
    @yield('page-content')
</div>
