<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        var APP_URL = "<?php echo url('/'); ?>";
    </script>
    <script src="<?php echo asset('/') . 'vendor/auth/public-ui/js/jquery.min.js';  ?>"></script><!-- 2.1.3v -->
    <!-- <script src="<?php echo asset('/') . 'vendor/auth/public-ui/js/jquery-3.4.1.slim.min.js';  ?>" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->

    <script src="<?php echo asset('/') . 'vendor/auth/public-ui/js/bootstrap.min.js';  ?>" ></script>
    <!-- Fonts -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:200,300,400,600%7CRoboto:300,400&amp;display=swap" media="all"> -->
    <link rel="stylesheet" href="<?php echo asset('/') . 'vendor/auth/public-ui/css/bootstrap.min.css';  ?>" >

    <!-- For Listing Slider -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('/') . 'vendor/auth/public-ui/css/swiper.min.css';  ?>"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo asset('/') . 'vendor/auth/public-ui/css/easyzoom.css';  ?>"> -->

    <!-- END Listing Slider -->

    <!-- <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}"> -->
    <link href="<?php echo asset('/') . 'vendor/auth/public-ui/css/custom.css';  ?>" rel="stylesheet">

    <!-- <script src="{{ asset('js/tinymce.min.js') }}" referrerpolicy="origin"></script> -->
    <!-- <link rel="stylesheet" href="{{asset('/jquery-ui-1.12.1/jquery-ui.css')}}"> -->
    <!-- <script src="{{asset('/jquery-ui-1.12.1/jquery-ui.js')}}"></script> -->

    <script src="<?php echo asset('/') . 'vendor/auth/public-ui/js/sm.js';  ?>" crossorigin="anonymous"></script>

    {{-- Multi select dropdown js / css --}}
    <!-- <link href="{{ asset('css/chosen.css') }}"  rel="stylesheet"> -->
    <!-- <script src="{{asset('js/chosen.jquery.js')}}"></script> -->
    {{-- <script src="{{ asset('js/common.js') }}"></script> --}}

    <!-- Google Api -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_QNJ9wsUGcYf2uLotBmTprhwCM0f3ycE&libraries=places"></script> -->
    <!-- <script type="text/javascript" src="<?php echo asset('/') . 'vendor/auth/public-ui/js/google-location.js'; ?>"></script> -->
    <!-- <script type="text/javascript">
        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"a2c6ea68ad7c45909f57fd92e7567b26943231319abcf1c85df7c93034c2146b0bed0e499410a25afb70ba801342bcbe", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
    </script> -->

    <!-- <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-63935099-6', 'auto');
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');
    </script> -->

    @yield('extra-css')
    @yield('extra-js')
</head>

<!-- END Google Api -->
<body  onscroll="myFunction()" class="header">
    <div class="loader " style="display: none;"></div>
    <nav id="topnavbar" class="navbar navbar-expand-lg topbar background_orange">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto align-items-center font_14px">
                <li class="nav-item active">
                    <a class="nav-link topbar-item" href="#"><i class="fa fa-mobile" style="font-size: 19px;" aria-hidden="true"></i> App download </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link topbar-item" href="tel:{{ __('app.PHONE_NO') }}"><i class="fa fa-phone" aria-hidden="true"></i> {{ __('app.PHONE_NO') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link topbar-item" href="mailto:sales@sharedmachine.in" class="mailto"><i class="fa fa-envelope-o" aria-hidden="true"></i> sales@sharedmachine.in</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link topbar-item" href="#"><i class="fa fa-language" aria-hidden="true"></i> {{ __('app.APP_LANG') }} &#9662;</a>
                    <ul class="dropdown">
                        <li class="languaage_li"><a class="text_black" href="{{ url('locale/en') }}">English </a></li>
                        <li class="languaage_li"><a class="text_black" href="{{ url('locale/hn') }}">Hindi </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>



    <nav id="first_menu" class="navbar navbar-expand-lg navbar-light bg-light p-0 navbar-sm-menus sticky-top" style="border-bottom: 0.5px solid #262020;">
        <a class="navbar-brand topbar-item p-0" href="{{url('/')}}" style="margin-left: 10px;">
            <img src="{{asset('/') . 'vendor/auth/public-ui/images/'}}logo.png">
        </a>
        <button class="navbar-toggler" id="top_menu_toggle" onclick="topMenuToggle()" data-toggle_value="on" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-2" aria-controls="navbarSupportedContent-2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-2" style="margin-left: 20px;">
            <div class="background_orange mobile_login_menu" style="height: 40px;width: 208px;margin-left: -15px;margin-top: -1px;box-shadow: 1px 2px 3px;">
                @if (isset(auth()->user()->profile_pic) && !empty(auth()->user()->profile_pic))
                    <span class="text_black font_weight400" style="margin-top: 10px;margin-left: 35px;">Hello, {{ Auth::user()->name }}</span>
                @else
                    <a class="top_menu" href="{{url('/login')}}" style="margin-left: 20px;">
                        <i class="fa fa-user" style="margin-top: 10px;"> <span style="padding-left: 25px;">Login</span></i>
                    </a>
                @endif


            </div>

            <div class="footer_menu_bottom_border"></div>

            <ul class="navbar-nav mr-auto sm_menu">
                <!-- Authentication Links -->


                @if (isset(auth()->user()->profile_pic) && !empty(auth()->user()->profile_pic))
                    <li class="nav-item menu-item active mobile_login_menu">
                        <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();localStorage.clear();document.getElementById('logout-form').submit();">
                            <img style="height: 30px;margin-left: 7px;margin-right: 7px;" src="<?php echo asset('/') . 'vendor/auth/public-ui/images/Logut1.png'; ?>">
                            <span class="text_black font_weight400 pointer"> {{ __('Logout') }}</span>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endif

                <div class="mobile_footer_menu">
                    <div class="footer_menu_top_border"></div>
                    <div class="" style="margin-top: 10px;font-size: 14px;font-weight: 400;">
                        <li id="about_sh_menu">
                            <span class="text_black font_weight400 pointer">About Shared Machines &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
                        </li>
                    </div>
                    <div class="footer_menu_bottom_border"></div>
                </div>
            </ul>
        </div>

         <div class="collapse footer_navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item menu-item active" id="footer_back_icon" style="margin-top: 5px;">
                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                    <span class="text_black font_weight400 pointer">About Shared Machines</span>
                </li>
                <div class="aboutSM_menu_bottom_border"></div>
                <li class="nav-item menu-item ml_30" style="margin-top: 5px;">
                    <span class="font_weight400 pointer">Information</span>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/about-us')}}">
                        <span class="text_black font_weight400 pointer">About</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/careers')}}">
                        <span class="text_black font_weight400 pointer">Career</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/partner-with-us')}}">
                        <span class="text_black font_weight400 pointer">Partner With Us</span>
                     </a>
                </li>
                <div class="aboutSM_menu_bottom_border_2"></div>

                <li class="nav-item menu-item ml_30" style="margin-top: 5px;">
                    <span class="font_weight400 pointer">Policies</span>
                </li>
                <li class="nav-item menu-item ml_30">
                    <a class="" href="{{url('/terms_condition')}}">
                        <span class="text_black font_weight400 pointer">Terms & Conditions</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/privacy-policy')}}">
                        <span class="text_black font_weight400 pointer">Privacy Policy</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/rental_terms')}}">
                        <span class="text_black font_weight400 pointer">Rental terms</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/subscription_terms')}}">
                        <span class="text_black font_weight400 pointer">Subscription terms</span>
                     </a>
                </li>
                <div class="aboutSM_menu_bottom_border_2"></div>

                <li class="nav-item menu-item ml_30" style="margin-top: 5px;">
                        <span class="font_weight400 pointer">Support</span>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/faq')}}">
                        <span class="text_black font_weight400 pointer">FAQ</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/contact-us')}}">
                        <span class="text_black font_weight400 pointer">Contact Us</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/forum')}}">
                        <span class="text_black font_weight400 pointer">Forum</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/knowledge-base')}}">
                        <span class="text_black font_weight400 pointer">Knowledge Base</span>
                     </a>
                </li>
                <div class="aboutSM_menu_bottom_border_2"></div>

                <li class="nav-item menu-item ml_30">
                    <span class="font_weight400 pointer">My Account</span>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/my_dashboard')}}">
                        <span class="text_black font_weight400 pointer">My Dashboard</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/refer-a-friend')}}">
                        <span class="text_black font_weight400 pointer">Refer & Earn</span>
                     </a>
                </li>
                <li class="nav-item menu-item ml_30">
                     <a class="" href="{{url('/partner-dashboard')}}">
                        <span class="text_black font_weight400 pointer">Partner Dashboard</span>
                     </a>
                </li>
                <div class="aboutSM_menu_bottom_border_2"></div>

            </ul>
        </div>

        <?php
            // added by shrikant 11-Sept-20.
            $session_data = session()->get('user_info');
            $url = 'my_dashboard';

            if(isset($session_data) && !empty($session_data)){
                switch ($session_data['company_id']) {
                    case 0:
                        $url = 'super_admin';break;
                    case 5:
                        $url = 'collab_team';break;
                    case $session_data['company_id'] > 1000:
                        $url = 'my_dashboard'; break;
                }
            }
        ?>

        <div class="collapse navbar-collapse" id="navbarSupportedContent1" style="padding: 0px 17px;">
            <ul class="navbar-nav ml-auto align-items-center">
                <li class="nav-item  menu-item active">
                    <a class="btn my-2 my-sm-0 btn-sm background_orange text_white" href="{{url($url)}}" style="padding: 1px 5px 0px 5px;">My Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset(auth()->user()->profile_pic) && !empty(auth()->user()->profile_pic))
                            <img src="{{ File::exists(base_path('/' . auth()->user()->profile_pic))? url(auth()->user()->profile_pic): url('vendor/auth/public-ui/images/profile.png') }}" style="width: 24px; height: 24px; border-radius: 50%;">
                            <span style="color:black;font-weight:400;">{{ Auth::user()->name }}</span>
                        @else
                            <i class="fa fa-user text_black" aria-hidden="true"></i>
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        @guest
                            <a class="dropdown-item" href="{{URL::to('/login')}}">Login</a>
                            <a class="dropdown-item" href="{{URL::to('/register')}}">Registration</a>
                        @else
                            <a class="dropdown-item" href="{{ url("user_profile") }}">
                               <img src="{{asset('/vendor/auth/public-ui/images/my_profile.png')}}"> &nbsp; Profile
                            </a>

                            <a class="dropdown-item" href="{{url($url)}}">
                               <img src="{{asset('/vendor/auth/public-ui/images/my_dashboard.png')}}"> &nbsp; Dashboard
                            </a>

                            <a class="dropdown-item" href="{{ url('my_wishlist') }}">
                                <img src="{{asset('/vendor/auth/public-ui/images/favourite_icon.png')}}"> &nbsp; Wishlist
                            </a>


                             <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();localStorage.clear();
                                    document.getElementById('logout-form').submit();">
                                <img src="{{asset('/vendor/auth/public-ui/images/logout.png')}}"> &nbsp; {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="page-content">
        @yield('page-content')
    </div>

    <div class="web_footer mt_50px">
        <div class="container-fluid background_orange">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('/vendor/auth/public-ui/images/location_new.png')}}">
                        </div>
                        <div class="col-md-8 text-center text-md-left px-0">
                            <span class="social-media font_18px">{{ __('app.OFFICE_ADDRESS') }} :</span>
                            <span class="social-media">{{ __('app.ADDRESS_LINE1') }}<br>{{ __('app.ADDRESS_LINE2') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('/vendor/auth/public-ui/images/phone.png')}}">
                        </div>
                        <div class="col-md-8 text-center text-md-left px-0" style="margin-top: 15px;">
                            <span class="social-media font_18px">{{ __('app.OFFICE_PHONE_NO') }} : </span>
                            <span class="social-media"><a href="tel:{{ __('app.PHONE_NO') }}" class="social-media remove-text-decoration">{{ __('app.PHONE_NO') }}</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('/vendor/auth/public-ui/images/mail.png')}}">
                        </div>
                        <div class="col-md-8 text-center text-md-left px-0" style="margin-top: 15px;">
                            <span class="social-media font_18px">{{ __('app.OFFICE_EMAIL') }} :</span>
                            <span class="social-media"><a href="mailto:sales@sharedmachine.in" class="social-media remove-text-decoration">sales@sharedmachine.in</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('/vendor/auth/public-ui/images/social_link.png')}}">
                        </div>
                        <div class="col-md-8 text-center text-md-left px-0">
                            <div class="social">
                                <a href="https://www.facebook.com/sharedmachine" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/facebook.png')}}" style="height: 33px;">
                                </a>
                                <a href="https://twitter.com/sharedmachine?s=11" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/twitter.png')}}" style="height: 33px;">
                                </a>
                                <a href="https://wa.me/message/JXL6NIKGRFJBE1" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/whatsapp.png')}}" style="height: 33px;">
                                </a>
                                <a href="https://www.linkedin.com/company/sharedmachine" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/linkdin.png')}}" style="height: 33px;">
                                </a>
                                <a href="https://www.instagram.com/sharedmachine_india" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/instragram.png')}}" style="height: 33px;">
                                </a><br>
                                <a href="https://g.page/SharedMachine?gm" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/googlebusiness.png')}}" style="height: 33px;">
                                </a>
                                <a href="https://t.me/SharedMachine_India" target="_blank">
                                    <img class="social_icon" src="{{asset('/vendor/auth/public-ui/images/telegram.png')}}" style="height: 33px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom: 35px;">
            <div class="main-footer row">
                <div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
                    <div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC1') }}</b> </div>
                    <div class="footer-list-item" style="margin: -15px 0px 0px 0px;"> <img src="{{asset('/vendor/auth/public-ui/images/leftline.png')}}"> </div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/about-us')}}">{{ __('app.FOOTER_SEC1_TEXT1') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/how-its-work')}}">{{ __('app.FOOTER_SEC1_TEXT2') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/careers')}}">{{ __('app.FOOTER_SEC1_TEXT3') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/partner-with-us')}}">{{ __('app.FOOTER_SEC1_TEXT4') }} </a></div>
                </div>

                <div class="col-12 col-sm-6 col-md main-footer-content justify-content-center" style="margin-left: 20px;">
                    <div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC2') }}</b> </div>
                    <div class="footer-list-item" style="margin: -15px 0px 0px 0px;"> <a href="#"><img src="{{asset('/vendor/auth/public-ui/images/leftline.png')}}"> </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/terms_condition')}}">{{ __('app.FOOTER_SEC2_TEXT1') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/privacy-policy')}}">{{ __('app.FOOTER_SEC2_TEXT2') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/rental_terms')}}">{{ __('app.FOOTER_SEC2_TEXT3') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/subscription_terms')}}"> {{ __('app.FOOTER_SEC2_TEXT4') }}</a></div>
                </div>

                <div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
                    <div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC3') }}</b> </div>
                    <div class="footer-list-item" style="margin: -15px 0px 0px 0px;"> <a href="#"><img src="{{asset('/vendor/auth/public-ui/images/leftline.png')}}"> </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/faq')}}">{{ __('app.FOOTER_SEC3_TEXT1') }}</a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/contact-us')}}">{{ __('app.FOOTER_SEC3_TEXT2') }}</a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/forum')}}">{{ __('app.FOOTER_SEC3_TEXT3') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/knowledge-base')}}">{{ __('app.FOOTER_SEC3_TEXT4') }}</a></div>
                </div>

                <div class="col-12 col-sm-6 col-md main-footer-content justify-content-center" style="margin-left: 12px;">
                    <div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC4') }}</b> </div>
                    <div class="footer-list-item" style="margin: -15px 0px 0px 0px;"> <a href="#"><img src="{{asset('/vendor/auth/public-ui/images/leftline.png')}}"> </a></div>
                    <?php
                        // added by shrikant 11-Sept-20.
                        $session_data = session()->get('user_info');
                        $login_url = 'login';

                        if(isset($session_data) && !empty($session_data)){
                            switch ($session_data['company_id']) {
                                case 0:
                                    $login_url = 'super_admin';break;
                                case 5:
                                    $login_url = 'collab_team';break;
                                case $session_data['company_id'] > 1000:
                                    $login_url = 'my_dashboard'; break;
                            }
                        }
                    ?>
                    <div class="footer-list-item"> <a class="text_black" href="{{url($login_url)}}">{{ __('app.FOOTER_SEC4_TEXT1') }}</a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{url($url)}}">{{ __('app.FOOTER_SEC4_TEXT2') }}</a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{url('/refer-a-friend')}}">{{ __('app.FOOTER_SEC4_TEXT3') }} </a></div>
                    <div class="footer-list-item"> <a class="text_black" href="{{URL::to('/partner-dashboard')}}">{{ __('app.FOOTER_SEC4_TEXT4') }}</a></div>
                </div>

                <div class="col-12 col-sm-6 col-md main-footer-content justify-content-center" style="margin-left: -20px;">
                    <div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC5') }}</b> </div>
                    <div class="footer-list-item" style="margin: -15px 0px 0px 0px;"> <img src="{{asset('/vendor/auth/public-ui/images/leftline.png')}}"> </div>
                    <div class="footer-list-item"> <img src="<?php echo asset('/') . 'vendor/auth/public-ui/images/';  ?>barcode.jpg" style="height: 80px;"></div>
                    <div class="footer-list-item" style="padding-top: 50px;"> <a class="text_black" href="https://play.google.com/store/apps/details?id=com.sharedmachine.mobile" target="_blank" class="cursor"><img src="{{asset('/vendor/auth/public-ui/images/Google-Play.png')}}"></a></div>
                    <div class="footer-list-item" style="padding-top: 30px;"> <a class="text_black" href="{{URL::to('/app_coming_soon')}}" target="_blank" class="cursor"><img src="{{asset('/vendor/auth/public-ui/images/App-store.png')}}"></a></div>
                    <div class="footer-list-item"> </div>

                </div>
            </div>
        </div>
        <div class="container text-center" style="margin-bottom: 20px;">
            <span class="font_14px text_black Copyright">© Copyright 2020 Sharedmachine. All Rights Reserved</span>
        </div>
    </div>

    {{-- Mobile Footer--}}
    <div id="mobile_footer" class="mobile_footer">
        <div class="footer_border"></div>
        <div class="row" style="margin-top: 5px;">
            <div class="col-1">
                <img src="{{asset('/') . 'vendor/auth/public-ui/images/'}}phone.png"
                style="height: 40px;">
            </div>
            <div class="col-5 footer_info">
                <span class="social-media"><a href="tel:{{ __('app.PHONE_NO') }}" class="social-media remove-text-decoration">{{ __('app.PHONE_NO') }}</a></span>
            </div>
            <div class="col-4">
                <a class="text_black" href="https://play.google.com/store/apps/details?id=com.sharedmachine.mobile" target="_blank" class="cursor">
                    <img class="play_store_icon" src="{{asset('/vendor/auth/public-ui/images/Google-Play.png')}}">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <img src="{{asset('/') . 'vendor/auth/public-ui/images/'}}mail.png"
                style="height: 40px;">
            </div>
            <div class="col-5 footer_info">
                <span class="social-media"><a href="mailto:sales@sharedmachine.in" class="social-media remove-text-decoration">sales@sharedmachine.in</a></span>
            </div>
            <div class="col-4">
                <a class="text_black" href="{{URL::to('/app_coming_soon')}}" target="_blank" class="cursor">
                    <img class="play_store_icon" src="{{asset('/vendor/auth/public-ui/images/App-store.png')}}">
                </a>
            </div>
        </div>
        <div class="footer_border_2"></div>
        <div class="row">
            <div class="social">
                <a href="https://www.facebook.com/sharedmachine" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/'}}facebook.png">
                </a>
                <a href="https://twitter.com/sharedmachine?s=11" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/'}}twitter.png">
                </a>
                <a href="https://wa.me/message/JXL6NIKGRFJBE1" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/'}}whatsapp.png">
                </a>
                <a href="https://www.linkedin.com/company/sharedmachine" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/'}}linkdin.png">
                </a>
                <a href="https://www.instagram.com/sharedmachine_india" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/'}}instragram.png">
                </a>
                <a href="https://g.page/SharedMachine?gm" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/googlebusiness.png'}}">
                </a>
                <a href="https://t.me/SharedMachine_India" target="_blank">
                    <img class="social_icon" src="{{asset('/') . 'vendor/auth/public-ui/images/telegram.png'}}">
                </a>

            </div>
        </div>
        <div class="container text-center" style="margin-bottom: 70px;">
            <span class="font_14px text_black font_weight500 Copyright">© Copyright 2020 Sharedmachine. All Rights Reserved</span>
        </div>
    </div>


    <button id="go_top" class="d-none"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>

    <script>
        const btnScrollToTop = document.querySelector("#go_top");
        btnScrollToTop.addEventListener("click", function(){
            document.body.scrollTop = 0, smooth;
        });
    </script>
     @yield('extra-footer-js')
</body>
</html>
