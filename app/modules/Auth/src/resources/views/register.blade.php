@extends('auth::public-ui.includes.public-layout')

@section('page-content')
<section id="register" class="my-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-center border-right">
                <h6 class="text-center font_weight500 text_black font_18px">Everything about Construction Machines</h6>
                <p class="text-center text_black font_13px">
                    Get or give construction Machines on rent,<br>
                    used sale,new sale or job work
                </p>
                <img class="text-center" width="60%" src="<?php echo asset('/').'vendor/auth/public-ui/images/';  ?>register.png">
            </div>
            <div class="col-md-4 text-center pl-4">
                <img src="<?php echo asset('/').'vendor/auth/public-ui/images/';  ?>SM_logo-(2).png" width="150px">
                <form id="regidter_form" method="POST" action="{{ url('register-user') }}" class="mt-3" onsubmit="validateCaptcha1()">
                    @csrf
                    <input type="text" name="registration_prevention" class="d-none" >
                    <div class="form-group">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-Mail Address">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="mobile_no" type="text" class="form-control mobile_number_validation @error('mobile_no') is-invalid @enderror" name="mobile_no" value="{{ old('mobile_no') }}" required autocomplete="mobile_no" placeholder="Phone Number" maxlength="10" minlength="10">
                        @error('mobile_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="firm_name" type="text" class="form-control @error('firm_name') is-invalid @enderror" name="firm_name" value="{{ old('firm_name') }}" autocomplete="firm_name" placeholder="Firm Name">
                        @error('firm_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div id="captcha" class="text-center"></div>
                        <input type="text" class="form-control mt-1 @error('captcha_input') is-invalid @enderror" name="captcha_input" required placeholder="Captcha" id="cpatchaTextBox"/>
                        @error('captcha_input')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-sky-blue mt-1 px-5 font_13px">Sign Up</button>
                </form>
                <div class="text-left mt-2">
                    <p class="text-left text_black font_14px">Already Have Account? <a href="<?php echo URL::to('/login');?>" class="t_color">Sign in</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $( document ).ready(function() {
        createCaptcha();

        $('#regidter_form').submit(function() {
            $('#regidter_form').append(
                '<input type="hidden" name="captcha" value="' + code + '">'
            );
        });
    });
</script>
<script type="text/javascript">
    var code;
    function createCaptcha() {
        //clear the contents of captcha div first
        document.getElementById('captcha').innerHTML = "";
        var charsArray = "0123456789";
        var lengthOtp = 6;
        var captcha = [];
        for (var i = 0; i < lengthOtp; i++) {
            //below code will not allow Repetition of Characters
            var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
            if (captcha.indexOf(charsArray[index]) == -1)
                captcha.push(charsArray[index]);
            else i--;
        }
        var canv = document.createElement("canvas");
        canv.id = "captcha";
        canv.width = 100;
        canv.height = 50;
        var ctx = canv.getContext("2d");
        ctx.font = "25px Georgia";
        ctx.strokeText(captcha.join(""), 0, 30);
        //storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join("");
        document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
    }

    function validateCaptcha() {
        event.preventDefault();
        if (document.getElementById("cpatchaTextBox").value != code) {
            alert("Invalid Captcha. try Again");
            createCaptcha();
        } else {
            document.getElementById("regidter_form").submit();
        }
    }
</script>
{{-- @endsection --}}
@stop
